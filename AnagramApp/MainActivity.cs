﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Content.Res;

namespace AnagramApp
{
    [Activity(Label = "AnagramApp", MainLauncher = true, Theme = "@style/splashscreen")]
    public class MainActivity : Activity
    {
        AnagramsAdapter _adapter;
        EditText _editTextView;

        private void SearchEditBoxTextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            _adapter.Filter.InvokeFilter(_editTextView.Text);
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.Window.RequestFeature(WindowFeatures.ActionBar);
            base.SetTheme(Resource.Style.MainTheme);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            AssetManager assets = this.Assets;

            // Dictionary words are stored in word.txt
            _adapter = new AnagramsAdapter(this, assets.Open("words.txt"));
            _editTextView = FindViewById<EditText>(Resource.Id.editText);

            // Everytime the text input changes, I invoke the custom filter to 
            // get the anagrams of the current character sequence.
            _editTextView.TextChanged += SearchEditBoxTextChanged;

            //anagramItemsListView is populated using the Anagrams Adapter
            var anagramItemsListView = FindViewById<ListView>(Resource.Id.anagramItemsList);
            anagramItemsListView.Adapter = _adapter;
        }
    }
}

