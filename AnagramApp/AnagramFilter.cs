﻿using System.Collections.Generic;
using Android.Widget;
using Java.Lang;

namespace AnagramApp
{
    public class AnagramFilter : Filter
    {
        private readonly AnagramsAdapter _anagramAdapter;
        public AnagramFilter(AnagramsAdapter adapter)
        {
            _anagramAdapter = adapter;
        }

        protected override FilterResults PerformFiltering(ICharSequence constraint)
        {
            var returnObj = new FilterResults();
            if (constraint != null)
            {
                if (_anagramAdapter.GetAnagrams(constraint.ToString(), out List<string> list))
                {
                    var results = list.ToArray();
                    returnObj.Values = results;
                    returnObj.Count = results.Length;
                }
                constraint.Dispose();
            }

            return returnObj;
        }

        protected override void PublishResults(ICharSequence constraint, FilterResults results)
        {
            // Update the filteredList in the adapter and signal that we have new results for the view.
            _anagramAdapter.UpdateResults((string[])results.Values);
            _anagramAdapter.NotifyDataSetChanged();

            if (constraint != null)
            {
                constraint.Dispose();
            }

            if(results != null)
            {
                results.Dispose();
            }
        }
    }
}