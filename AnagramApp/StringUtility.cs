﻿using System;

namespace AnagramApp
{
    public static class StringUtility
    {
        public static string SortString(string str)
        {
            char[] foo = str.ToCharArray();
            Array.Sort(foo);
            return new string(foo);
        }
    }
}