﻿using System.Collections.Generic;
using Android.Views;
using Android.Widget;
using System.IO;
using System;
using Java.Lang;

namespace AnagramApp
{
    public class AnagramsAdapter : BaseAdapter, IFilterable
    {
        // Key: loweredcased sorted strings, Value: List of all possible anagrams of the key string
        private Dictionary<string, List<string>> _anagramsMap;
        // Anagram Filter updates this list after PerformFiltering
        private List<string> _filteredList;
        private MainActivity _mainActivity;
        private Stream _dictionaryStream;
        private Filter _filter;

        public AnagramsAdapter(MainActivity mainActivity, Stream stream)
        {
            this._mainActivity = mainActivity;
            this._dictionaryStream = stream;
            _filter = new AnagramFilter(this);
            _filteredList = new List<string>();
            PopulateAnagramsMap();
        }

        public override int Count => _filteredList.Count;

        public Filter Filter => _filter;

        public override Java.Lang.Object GetItem(int position)
        {
            return _filteredList[position];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if (view == null)
            {
                view = _mainActivity.LayoutInflater.Inflate(Android.Resource.Layout.SimpleListItem1, null);
            }
            view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = _filteredList[position];
            return view;
        }

        // The input string is converted to lowercase and then sorted before we try and get the list of anagrams
        // since thats how the map is constructed.
        internal bool GetAnagrams(string v, out List<string> list) => _anagramsMap.TryGetValue(StringUtility.SortString(v.ToLower()), out list);

        internal void UpdateResults(string[] values)
        {
            _filteredList.Clear();
            if (values != null)
            {
                _filteredList.AddRange(values);
            }
        }

        //PopulateAnagramgs
        // Implementation: The anagrams of a word is a word, phrase, or name formed by rearranging the letters 
        // of another. To make it easy and fast to retrieve all the anagrams of a word, we map every word value 
        // to the sorted version of the word as the key. The intention of constructing the map this way, is to 
        // be able to sort any input word and quickly retrieve a list of all possible anagrams.
        //
        //1. Read every word from the dictionary file.
        //2. Convert each word string to lowercase since the keys of _anagramsMap are not case-sensitive.
        //3. Sort the string before its inserted or before the map is queried.
        //4. If the key exists, the original word is added to the list to indicate that the original word
        //   is an anagram of the key string. Else, we insert the sorted string as a new key.
        private void PopulateAnagramsMap()
        {
            _anagramsMap = new Dictionary<string, List<string>>();
            var file = new System.IO.StreamReader(_dictionaryStream, System.Text.Encoding.UTF8, true, 128);
            string word;
            while ((word = file.ReadLine()) != null)
            {
                var sortedString = StringUtility.SortString(word.ToLower());
                if (!_anagramsMap.ContainsKey(sortedString))
                {
                    _anagramsMap.Add(sortedString, new List<string>());
                }
                _anagramsMap[sortedString].Add(word);
            }
        }
    }
}